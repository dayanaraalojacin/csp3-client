# csp3-client
The client application of the Money Tracker project.

## Getting Started
- Clone this project to your local machine.
```
$ git clone <project_uri>
```

- Go to the root directory of this project.
- Install all dependencies
```
$ npm install
```

- Run the application

```
$ npm run dev
```