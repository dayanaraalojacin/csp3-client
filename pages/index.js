import Head from 'next/head'
import Image from 'next/image'
import styles from '../styles/Home.module.css'

export default function Home() {
  return (
    
    <div className={styles.container}>
        {/* <div className="custom-shape-divider-bottom-1622874907">
        <svg data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1200 120" preserveAspectRatio="none">
        <path d="M1200 120L0 16.48 0 0 1200 0 1200 120z" className="shape-fill">
        </path>
        </svg>
        </div> */}

    <Head>
        <title>Money Tracker &middot; Welcome to the Money Tracker project</title>
    </Head>
    
    <div className={styles.jumbotron}>
      <div className={styles.herocontent}>
        <h1>Welcome to the Money Tracker project.</h1>
        <p>Keep track of your income, expenses, and net worth.</p>
        <p>Budget your finances to know where your money is going. </p>
        {/* <p>MoneyTracker is a complete online money management tool designed to keep track of all your transactions and bank accounts from your computer and mobile phone</p> */}
        <a href="./courses.html" class="btn btn-danger"> Get Started! </a>
        
      
        </div>

      

        </div>

      

        </div>
   
  )
}
