import 'bootstrap/dist/css/bootstrap.min.css' //Integrate bootstrap 4
import '../styles/globals.css'
import Layout from './components/Layout'


function MyApp({ Component, pageProps }) {
  return (
  <Layout>
    <Component {...pageProps} />
  </Layout>
  )
}

export default MyApp
