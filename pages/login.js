import Head from 'next/head';
import styles from '../styles/Login.module.css';
import { useState } from 'react';
import swal from 'sweetalert2';
import router from 'next/router';

export const getStaticProps = async () => {
  return {
    props: {
      API_URI: process.env.API_URI
    }
  }
};

export default function Login({ API_URI }) {
  const [emailAddress, setEmailAddress] = useState('');
  const [password, setPassword] = useState('');
  const [tokenId, setTokenId] = useState(null);

  
  const getUserDetails = (token) => {
    fetch(`${API_URI}/api/users/details`, {
      headers: {
        'Authorization': `Bearer ${token}`
      }
    })
      .then(res => res.json())
      .then(data => {
        console.dir(data);
      })
  };

  const login = (e) => {
    e.preventDefault(); // Do not allow the page to refresh

    fetch(`${API_URI}/api/users/login`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        emailAddress: emailAddress,
        password: password
      })
    })
      .then(res => res.json())
      .then(data => {
        // If login was successful:
        if (data.data) {
          if (typeof(data.token) !== 'undefined') {
              localStorage.setItem('token', data.token);
            // display a sweet alert
              swal.fire({
                icon: 'success',
                title: 'Successful Login',
                text: 'Welcome back!'
                });
                // then, redirect the user to the /transactions page
                router.push('/transactions');
                // Retrieve the user details
                getUserDetails(localStorage.getItem('token'));
          }
        }
      });
  };

  return (
    <div className={styles.container}>
      <Head>
        <title>MoneyTracker &middot; Log In</title>
      </Head>
      <div className="container my-5">
        {/* <h2 className="text-center my-4 login">Log in to your existing account</h2> */}

      </div>
        <div className="row">
          <div className="col-md-6 offset-md-3">

            <form id="login-form" className="card1 login-body bg-light" onSubmit={e => login(e) }>
            <div className={styles.imgcontainer}>
              <img src="/images/login.png" alt="Avatar" className={styles.avatar}></img>
            </div>

            <div className="form-group">
            <label htmlFor="inputEmailAddress">Email Address</label>
              <input
                type="email"
                id="email-address"
                className="form-control mb-3"
                placeholder="Enter your email address"
                onChange={e => setEmailAddress(e.target.value)}
                value={emailAddress}
              />
              
              </div>

              <div className="form-group">
              <label htmlFor="inputPassword">Password</label>
              <input
                type="password"
                id="password"
                className="form-control mb-3"
                placeholder="Enter your account password"
                onChange={e => setPassword(e.target.value)}
                value={password}
              />
              </div>

              <button type="submit" className="btn btn-block btn-danger">
                Log In 
              </button>

              <a href="/register" className="btn btn-block text-primary">
                Not yet registered? Create a new account now!
              </a>
            </form>
          </div>
        </div>
      </div>
  )
}