import Link from 'next/link'
import Image from 'next/image'

const NavBar = () => {
  return (
 <nav>
   <div className="logo ">
     <h1 className="brand-name">
       <Link href="/">MoneyTracker</Link>
       <Image className="brand-logo" src="/images/logo.png" width={50} height={50} />
     </h1>
   </div>

<Link href="/ " >
   <a>Home</a>
</Link>
<Link href="/transactions">
   <a>Transactions</a>
</Link>

<Link href="/profile">
   <a>My Profile</a>
</Link> 

<Link href="/register">
   <a>Register</a>
</Link>

<Link href="/login">
   <a>Login</a>
</Link>

<Link href="/">
   <a>Logout</a>
</Link>

 </nav>
  
  );
  
  };
  
  export default NavBar;