import Footer from "./Footer"
import NavBar from "./NavBar"


const Layout = ({children}) => {
return (
  <div className="content">
    <div className="containers">
    <NavBar />
    {children}
  <Footer/>
  </div>
  </div>
);
};

export default Layout;