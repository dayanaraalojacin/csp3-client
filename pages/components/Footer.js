
const Footer = () => {
return (
<footer className="content fixed-bottom">
  <p>Copyright &copy; 2021 by Dayanara Dalida &middot; All Rights Reserved.</p>
</footer>

);

};

export default Footer;