// pages/register.js

import Head from 'next/head';
import styles from '../styles/Register.module.css';
import { useEffect, useState } from 'react';
import router from 'next/router';
import swal from 'sweetalert2';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"; // Import the FontAwesomeIcon component
import { faSearch } from "@fortawesome/free-solid-svg-icons";




export const getStaticProps = async () => {
  return {
    props: {
      API_URI: process.env.API_URI
    }
  }
};

export default function Register({API_URI}) {
  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [emailAddress, setEmailAddress] = useState('');
  const [password, setPassword] = useState('');
  const [confirmPassword, setConfirmPassword] = useState('');
  const [submitDisabled, setSubmitDisabled] = useState(true);

  useEffect(() => {
    let isUserDetails = firstName !== '' && lastName !== '' && emailAddress !== '' && password !== '' && confirmPassword !== '';
    let isPasswordEqual = password === confirmPassword;
    let isPasswordLength = password.length >= 12;
    if (isUserDetails && isPasswordEqual && isPasswordLength) {
      setSubmitDisabled(false);
    } else {
      setSubmitDisabled(true);
    }
  }, [firstName, lastName, emailAddress, password, confirmPassword]);



  const register = (e) => {
    e.preventDefault();
    
    fetch(`${API_URI}/api/users/register`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        firstName,
        lastName,
        emailAddress,
        password
      })
    })
      .then(res => res.json())
      .then(data => {
        console.dir(data);


        if (data.data) {
          //Notify the user of successful registration
          swal.fire({
            icon: 'success',
            title: 'Successful Registration',
            'text': 'Thank you, registration of you new account was successful'
          })
          // Redirect the user to the login page
          router.push('/login');
        }
      });
    
      setFirstName('');
      setLastName('');
      setEmailAddress('');
      setPassword('');
      setConfirmPassword('');
  };

  return (
    <div className="active">


      <Head>
        <title>MoneyTracker &middot; Register</title>
      </Head>

      <div className="container my-5">
      <h2 className="text-center my-3">Register a new account</h2>
      <div className="row">
        <div className="col-md-6 offset-md-3">

          <form id="register-form" className="card1 login-body bg-light" onSubmit={e => register(e)}>
            <div className="form-group">
              <label htmlFor="inputFirstName">First Name</label>
              <input
                type="text"
                id="first-name"
                className="form-control "
                placeholder="Enter your first name"
                onChange={e => setFirstName(e.target.value)}
                value={firstName}
                required
              />
            </div>
            
            <div className="form-group">
            <label htmlFor="inputLastName">Last Name</label>
            <input
              type="text"
              id="last-name"
              className="form-control"
              placeholder="Enter your lastname"
              onChange={e => setLastName(e.target.value)}
              value={lastName}
              required
            />
            </div>

            <div className="form-group">
            <label htmlFor="inputEmailAddress">Email Address</label>
            <input
              type="email"
              id="email-address"
              className="form-control"
              placeholder="Enter your email address"
              onChange={e => setEmailAddress(e.target.value)}
              value={emailAddress}
              required
            />
            </div>

            <div className="form-row my-2">
              <div className="form-group col-md-6">
              <label htmlFor="inputPassword">Password</label>
                <input
                  type="password"
                  id="password"
                  className="form-control"
                  placeholder="Enter your password"
                  onChange={e => setPassword(e.target.value)}
                  value={password}
                  required
                />
              </div>

              <div className="form-group col-md-6">
              <label htmlFor="confirmPassword">Confirm Password</label>
                <input
                  type="password"
                  id="confirm-password"
                  className="form-control"
                  placeholder="Confirm your password"
                  onChange={e => setConfirmPassword(e.target.value)}
                  value={confirmPassword}
                  required
                />
              </div>
            </div>
            <button type="submit" className="btn btn-block btn-danger my-2" disabled={submitDisabled}>Register 
            


            </button>
          </form>
        </div>
      </div>
    </div>
    </div>
  );
}
