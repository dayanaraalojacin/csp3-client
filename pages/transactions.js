import Head from 'next/head'
import styles from '../styles/Transactions.module.css'

export default function Transactions() {
  return (
    <div className={styles.container}>
      <Head>
        <title>Money Tracker &middot; Transactions</title>
      </Head>

      <div>
        <h1>Transactions</h1>
      </div>

    </div>
  )
}
